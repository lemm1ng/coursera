/**
 * @param {Array} collection
 * @params {Function[]} – Функции для запроса
 * @returns {Array}
 */

var friends = [{
    name: 'Сэм',
    gender: 'Мужской',
    email: 'luisazamora@example.com',
    favoriteFruit: 'Картофель'
},
    {
        name: 'Эмили',
        gender: 'Женский',
        email: 'example@example.com',
        favoriteFruit: 'Яблоко'
    },
    {
        name: 'Мэт',
        gender: 'Мужской',
        email: 'danamcgee@example.com',
        favoriteFruit: 'Яблоко'
    },
    {
        name: 'Брэд',
        gender: 'Мужской',
        email: 'newtonwilliams@example.com',
        favoriteFruit: 'Банан'
    }
];

function query(collection, selectFunc, filterFunc) {
    return collection.filter(filterFunc).map(selectFunc);
}

function filterIn(field, allowedValues) {
    return function filterFunc(element) {
        return allowedValues.includes(element[field]);
    };
}


function select(...fields) {
    return function selectFunc(element) {
        return fields.reduce((acc, fieldName) => {
            acc[fieldName] = element[fieldName];
            return acc;
        }, {});
    };
}

/**
 * @param {String} property – Свойство для фильтрации
 * @param {Array} values – Массив разрешённых значений
 */

module.exports = {
    query: query,
    select: select,
    filterIn: filterIn
};
