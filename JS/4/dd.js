function normalizeHashTags (hashtags) {
    var arr = [];
    for (var i = 0; i < hashtags.length; i++) {
      if (arr.lastIndexOf(hashtags[i].toLowerCase()) == -1) {
          arr.push(hashtags[i].toLowerCase());
            }
        }
return arr.join(', ');
}


console.log(normalizeHashTags(['web', 'coursera', 'JavaScript', 'Coursera', 'script', 'programming'])),
    'web, coursera, javascript, script, programming',
'Список "web, coursera, JavaScript, Coursera, script, programming"' +
' содержит хэштеги "web, coursera, javascript, script, programming"'
;