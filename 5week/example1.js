module.exports = {

var subscribers = [];


    /**
     * @param {String} event
     * @param {Object} subscriber
     * @param {Function} handler
     */
    on: function (event, subscriber, handler) {
       var new_subr = {
		event : event;
		subscriber :  subscriber;
		handler :  handler;
	};
	subscribers.push(new_subr);

    },

    /**
     * @param {String} event
     * @param {Object} subscriber
     */
    off: function (event, subscriber) {
	for(var i=0; i< subscribers.length; i++)
	{
	  var s = subscribers[i];
	  if(s.even == event && s.subscriber.equals(subscriber) ){
		subscribers.delete(i)
	  }
	}

    },

    /**
     * @param {String} event
     */
    emit: function (event) {
	for(var i=0; i< subscribers.length; i++)
	{
	  var s = subscribers[i];
	  if(s.even == event){
		s.handler.call(s.subscriber);
	  }
	}

    }
};
